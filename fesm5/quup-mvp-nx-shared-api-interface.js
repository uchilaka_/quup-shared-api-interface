import { __extends } from 'tslib';
import { InjectionToken, EventEmitter, Output } from '@angular/core';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { createEntityAdapter } from '@ngrx/entity';

var AppRoute;
(function (AppRoute) {
    AppRoute["AUTH"] = "auth";
    AppRoute["ACCOUNT"] = "account";
    AppRoute["PRODUCT"] = "product";
    AppRoute["EVENTS"] = "events";
    AppRoute["WIZARD_EVENT"] = "wizard/event";
})(AppRoute || (AppRoute = {}));

var appFeatureRoutes = [
    {
        path: AppRoute.PRODUCT,
        loadChildren: '@quup-mvp-nx/feature-product-mvp#FeatureProductMvpModule'
    },
    {
        path: AppRoute.AUTH,
        loadChildren: '@quup-mvp-nx/shared/feature-auth#FeatureAuthModule'
    },
    /**
     * @TODO design and code the organization context for all routes below
     * this section
     */
    {
        path: AppRoute.ACCOUNT,
        loadChildren: '@quup-mvp-nx/shared/feature-account#FeatureAccountModule'
    },
    {
        path: AppRoute.EVENTS,
        loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
    }
];

/**
 * MUST Be implemented at the root routing level of the app to be able
 * to target the referenced app outlet
 */
var WizardRouteEvent = {
    path: AppRoute.WIZARD_EVENT,
    outlet: 'modal',
    loadChildren: '@quup-mvp-nx/feature-events#FeatureEventsModule'
};

var CmsObjectStatus;
(function (CmsObjectStatus) {
    CmsObjectStatus["PUBLISHED"] = "published";
    CmsObjectStatus["DRAFT"] = "draft";
})(CmsObjectStatus || (CmsObjectStatus = {}));

var DRAFT_DOC_INDEX = 'zDraftDocument';

var QxFirebaseAuthErrorCodes;
(function (QxFirebaseAuthErrorCodes) {
    QxFirebaseAuthErrorCodes["EMAIL_ALREADY_EXISTS"] = "auth/email-already-exists";
})(QxFirebaseAuthErrorCodes || (QxFirebaseAuthErrorCodes = {}));

// import * as moment from 'moment-timezone';
var TimeRecord = /** @class */ (function () {
    function TimeRecord(utc, unix) {
        if (utc === void 0) { utc = new Date().toISOString(); }
        if (unix === void 0) { unix = new Date().getTime(); }
        this.utc = utc;
        this.unix = unix;
    }
    // utc: string;
    // unix: number;
    TimeRecord.NOW = function (timezone) {
        if (timezone === void 0) { timezone = 'America/New_York'; }
        // const now = moment(new Date()).tz(timezone);
        return TimeRecord.sanitize(new TimeRecord());
    };
    TimeRecord.sanitize = function (_a) {
        var utc = _a.utc, unix = _a.unix;
        return { utc: utc, unix: unix };
    };
    return TimeRecord;
}());
var DURATION_INTERVALS = [
    { label: 'minutes', value: 1 },
    { label: 'hours', value: 60 },
    { label: 'days', value: 24 * 60 },
    { label: 'weeks', value: 7 * 24 * 60 },
    { label: 'months', value: 30 * 7 * 24 * 60 }
];

var COLL_PATH_USER_REF = 'userInfo';
var COLL_PATH_USERDATA_REF = 'userData';

var APP_CONFIG_TOKEN = new InjectionToken('App config');

var Dismissable = /** @class */ (function () {
    function Dismissable() {
        /**
         * Set dismissable property to true by default
         */
        this.dismissable = true;
        /**
         * Emit instance of component being dismissed
         */
        this.dismiss = new EventEmitter();
    }
    return Dismissable;
}());

var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse || (DocumentResponse = {}));
var InteractiveDocument = /** @class */ (function (_super) {
    __extends(InteractiveDocument, _super);
    function InteractiveDocument() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * All interactive documents are dismissable by default
         */
        _this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        _this.userResponse = new EventEmitter();
        return _this;
    }
    InteractiveDocument.propDecorators = {
        userResponse: [{ type: Output }]
    };
    return InteractiveDocument;
}(Dismissable));

var UIAlertType;
(function (UIAlertType) {
    UIAlertType["WARNING"] = "alert-warning";
    UIAlertType["SUCCESS"] = "alert-success";
    UIAlertType["ERROR"] = "alert-danger";
    UIAlertType["INFO"] = "alert-info";
    UIAlertType["HINT"] = "alert-light";
})(UIAlertType || (UIAlertType = {}));
var FormatTime;
(function (FormatTime) {
    FormatTime["LONG"] = "dddd, MMMM Do YYYY, h:mm:ss a z";
    FormatTime["SHORT"] = "ddd, MMM Do YYYY, h:mm a";
})(FormatTime || (FormatTime = {}));
var TimeZones;
(function (TimeZones) {
    TimeZones["EST"] = "America/New_York";
    TimeZones["WAT"] = "Africa/Lagos"; // +01:00
})(TimeZones || (TimeZones = {}));
var DEFAULT_TIME_ZONE = new InjectionToken('Default TimeZone');
var FORMAT_TIME_LONG = 'dddd, MMMM Do YYYY, h:mm:ss a';
var FORMAT_TIME_SHORT = 'ddd, MMM Do YYYY, h:mm a';
var UI_TOAST_ALERT_TYPE = UIAlertType.INFO;
var UI_TOAST_DISMISS_TIMEOUT = new InjectionToken('UI Toast Dismiss Timeout');
var MODAL_OVERLAY_REF = new InjectionToken('Modal OverlayRef');

var FEATURE_ACCOUNT_SLICE = 'account';
var initialFeatureAccountState = {
    records: []
};

var initialFeatureAuthState = {
    agreementRecords: []
};
var AUTH_FEATURE_KEY = 'auth';

var initialOrgState = {
    ids: [],
    entities: {}
};
var ORG_SLICE = 'organizations';

var initialSessionState = {
    startTime: new Date().getTime()
};

var TASKS_SLICE = 'tasks';
var initialTaskState = {
    taskCounter: 0,
    ids: [],
    entities: {}
};

var _a;
var initialAppState = (_a = {
        auth: initialFeatureAuthState,
        session: initialSessionState
    },
    _a[TASKS_SLICE] = initialTaskState,
    _a[FEATURE_ACCOUNT_SLICE] = initialFeatureAccountState,
    _a[ORG_SLICE] = initialOrgState,
    _a);

var FEATURE_EVENTS_ENTITY_STATE = 'event_list';
var FEATURE_EVENTS_SLICE = 'events';
var FEATURE_QUEUE_SLICE = 'queues';
var COLLECTION_SLICE = new InjectionToken('Collection Slice');
var compareEntityIdsAsString = function (a, b) {
    if (!a && !b) {
        throw new Error('At least 1 attribute MUST have a valid ID');
    }
    return a > b ? a.id : b.id;
};
var selectEntityIdAsString = function (model) {
    return model.id;
};

var initialCourseAliasState = {
    ids: [],
    entities: {},
    selectedCourseAliasId: null
};
var COURSE_ALIAS_SLICE = 'courses';

var ErrorActionProp = /** @class */ (function () {
    function ErrorActionProp(error, length) {
        if (length === void 0) { length = 0; }
        this.error = error;
        this.length = length;
    }
    return ErrorActionProp;
}());
function ErrorAction(error, length) {
    if (length === void 0) { length = 0; }
    return new ErrorActionProp(error, length);
}

/**
 * Shared (Backend + Frontend) models
 */

function sortCourseAliasById(e1, e2) {
    return e1.id - e2.id;
}
function sortCourseAliasIds(id1, id2) {
    return id1 - id2;
}
function reduceCourseAliasArrayToMap(map, courseAlias) {
    map[courseAlias.id] = courseAlias;
    return map;
}
var selectCourseAliasEntityId = function (courseAlias) {
    return courseAlias.id;
};
var courseAliasEntityAdapter = createEntityAdapter({
    sortComparer: sortCourseAliasById,
    selectId: selectCourseAliasEntityId
});

var compareEventByTimeCreated = function (a, b) {
    if (!a.time_created) {
        return !b.time_created ? 0 : b.time_created.unix;
    }
    else if (!b.time_created) {
        return !a.time_created ? 0 : a.time_created.unix;
    }
    return a.time_created.unix > b.time_created.unix
        ? b.time_created.unix
        : a.time_created.unix;
};
var interactiveEventEntityAdapter = createEntityAdapter({
    // sortComparer: compareEntityIdsAsString,
    sortComparer: compareEventByTimeCreated,
    selectId: selectEntityIdAsString
});
var selectInteractiveEventState = createFeatureSelector(FEATURE_EVENTS_SLICE);
var _a$1 = interactiveEventEntityAdapter.getSelectors(), selectAll = _a$1.selectAll, selectEntities = _a$1.selectEntities, selectIds = _a$1.selectIds;
var selectAllInteractiveEvents = createSelector(selectInteractiveEventState, function (state) { return selectAll(state); });
var selectInteractiveEventEntities = createSelector(selectInteractiveEventState, function (state) { return selectEntities(state); });
var selectInteractiveEventIds = createSelector(selectInteractiveEventState, function (state) { return selectIds(state); });

var orgEntityAdapter = createEntityAdapter({
    selectId: function (org) { return org.id; },
    sortComparer: function (org1, org2) { return (org1.id > org2.id ? org1.id : org2.id); }
});

var sortComparer = function (task1, task2) {
    return task1.id > task2.id ? task1.id : task2.id;
};
var ɵ0 = sortComparer;
var selectId = function (task) { return task.id; };
var ɵ1 = selectId;
var taskEntityAdapter = createEntityAdapter({
    sortComparer: sortComparer,
    selectId: selectId
});

var initialQueueEntityState = {
    ids: [],
    entities: {}
};
var interactiveQueueEntityAdapter = createEntityAdapter({
    sortComparer: compareEntityIdsAsString,
    selectId: selectEntityIdAsString
});

/**
 * Generated bundle index. Do not edit.
 */

export { AppRoute, appFeatureRoutes, WizardRouteEvent, CmsObjectStatus, DRAFT_DOC_INDEX, QxFirebaseAuthErrorCodes, TimeRecord, DURATION_INTERVALS, COLL_PATH_USER_REF, COLL_PATH_USERDATA_REF, APP_CONFIG_TOKEN, Dismissable, DocumentResponse, InteractiveDocument, UIAlertType, FormatTime, TimeZones, DEFAULT_TIME_ZONE, FORMAT_TIME_LONG, FORMAT_TIME_SHORT, UI_TOAST_ALERT_TYPE, UI_TOAST_DISMISS_TIMEOUT, MODAL_OVERLAY_REF, initialAppState, FEATURE_EVENTS_ENTITY_STATE, FEATURE_EVENTS_SLICE, FEATURE_QUEUE_SLICE, COLLECTION_SLICE, compareEntityIdsAsString, selectEntityIdAsString, initialFeatureAuthState, AUTH_FEATURE_KEY, FEATURE_ACCOUNT_SLICE, initialFeatureAccountState, initialCourseAliasState, COURSE_ALIAS_SLICE, TASKS_SLICE, initialTaskState, initialOrgState, ORG_SLICE, ErrorActionProp, ErrorAction, sortCourseAliasById, sortCourseAliasIds, reduceCourseAliasArrayToMap, selectCourseAliasEntityId, courseAliasEntityAdapter, compareEventByTimeCreated, interactiveEventEntityAdapter, selectInteractiveEventState, selectAllInteractiveEvents, selectInteractiveEventEntities, selectInteractiveEventIds, orgEntityAdapter, taskEntityAdapter, ɵ0, ɵ1, initialQueueEntityState, interactiveQueueEntityAdapter };

//# sourceMappingURL=quup-mvp-nx-shared-api-interface.js.map