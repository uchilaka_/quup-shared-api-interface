export declare enum AppRoute {
    AUTH = "auth",
    ACCOUNT = "account",
    PRODUCT = "product",
    EVENTS = "events",
    WIZARD_EVENT = "wizard/event"
}
