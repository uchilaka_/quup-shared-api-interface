import { EntityState, EntityAdapter } from '@ngrx/entity';
import { Queue } from '../models';
export interface QueueEntityState extends EntityState<Queue> {
    lastCandidatePIN?: string;
}
export declare const initialQueueEntityState: QueueEntityState;
export declare const interactiveQueueEntityAdapter: EntityAdapter<Queue>;
