import { EntityAdapter, EntityState } from '@ngrx/entity';
import { ComparerNum } from '@ngrx/entity/src/models';
import { InteractiveEvent } from '../models';
export interface InteractiveEventEntityState extends EntityState<InteractiveEvent> {
    selectedEventId?: string;
}
export declare const compareEventByTimeCreated: ComparerNum<InteractiveEvent>;
export declare const interactiveEventEntityAdapter: EntityAdapter<InteractiveEvent>;
export declare const selectInteractiveEventState: import("@ngrx/store").MemoizedSelector<object, InteractiveEventEntityState, import("@ngrx/store/src/selector").DefaultProjectorFn<InteractiveEventEntityState>>;
export declare const selectAllInteractiveEvents: import("@ngrx/store").MemoizedSelector<object, InteractiveEvent[], import("@ngrx/store/src/selector").DefaultProjectorFn<InteractiveEvent[]>>;
export declare const selectInteractiveEventEntities: import("@ngrx/store").MemoizedSelector<object, import("@ngrx/entity").Dictionary<InteractiveEvent>, import("@ngrx/store/src/selector").DefaultProjectorFn<import("@ngrx/entity").Dictionary<InteractiveEvent>>>;
export declare const selectInteractiveEventIds: import("@ngrx/store").MemoizedSelector<object, string[] | number[], import("@ngrx/store/src/selector").DefaultProjectorFn<string[] | number[]>>;
