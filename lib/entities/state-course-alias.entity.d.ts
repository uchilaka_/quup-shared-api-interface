import { EntityAdapter, EntityState } from '@ngrx/entity';
import { IdSelector } from '@ngrx/entity/src/models';
import { CmsCourseAlias, CourseAliasMap } from '../models';
export interface QxCourseAliasEntityState extends EntityState<CmsCourseAlias> {
    selectedCourseAliasId: number | null;
}
export declare function sortCourseAliasById(e1: CmsCourseAlias, e2: CmsCourseAlias): number;
export declare function sortCourseAliasIds(id1: number, id2: number): number;
export declare function reduceCourseAliasArrayToMap(map: CourseAliasMap, courseAlias: CmsCourseAlias): CourseAliasMap;
export declare const selectCourseAliasEntityId: IdSelector<CmsCourseAlias>;
export declare const courseAliasEntityAdapter: EntityAdapter<CmsCourseAlias>;
