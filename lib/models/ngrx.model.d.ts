export declare class ErrorActionProp {
    error?: Error;
    length: number;
    constructor(error?: Error, length?: number);
}
export declare function ErrorAction(error?: Error, length?: number): ErrorActionProp;
