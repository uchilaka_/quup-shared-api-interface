export interface CmsDomain {
    id: number;
    status: string;
    created_on: string;
    name: string;
}
export declare type ApiCmsDomain = Partial<CmsDomain>;
export declare enum CmsObjectStatus {
    PUBLISHED = "published",
    DRAFT = "draft"
}
export interface CmsObject {
    id: number;
    created_by: number;
    created_on: Date | string;
    status: CmsObjectStatus;
}
export interface CmsUser {
    id: number;
    email: string;
    status?: string;
    company?: string;
    first_name?: string;
    last_name?: string;
    token?: string;
    timezone?: string;
    password?: string;
    [key: string]: any;
}
export interface CmsCourse extends CmsObject {
    code: string;
    title: string;
    description?: string;
}
export interface CmsCourseAlias extends CmsObject {
    course: number;
    detail: string;
    organization: number;
    display_name: string;
}
export interface CmsOrgToEntityAliasMap {
    id: number;
    organization_id: number;
    entity_alias_id: number;
}
export interface CmsEntityAlias {
    id: number;
    name: string;
    alias: string;
    status: string;
}
export interface CmsOrg extends CmsObject {
    name: string;
    aliases?: {
        [key: string]: string;
    };
}
