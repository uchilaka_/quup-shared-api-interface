import { AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
export interface GenericDataService {
    Collection: <T>() => AngularFirestoreCollection<T>;
    exists: (...args: any) => Observable<boolean>;
    /**
     * @TODO revisit in unit testing to examine handling of
     * "data not available" use case
     */
    saveAsNew: <T>(data: T, authorUID?: string) => Observable<T> | Observable<null>;
    docAsRef: <T>(docId: string) => AngularFirestoreDocument<T>;
}
