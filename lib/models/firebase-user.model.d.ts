export interface FirebaseUserProfile {
    uid: string;
    displayName: string;
    email: string;
    emailVerified?: boolean;
    photoUrl?: string;
}
