import { QxCourseAliasEntityState } from '../entities';
import { CmsCourseAlias } from './cms-object.model';
export declare const initialCourseAliasState: QxCourseAliasEntityState;
export interface CourseAliasMap {
    [courseAliasId: string]: CmsCourseAlias;
}
export declare const COURSE_ALIAS_SLICE = "courses";
