export interface GenericFormInputOption {
    label: string;
    value: any;
}
