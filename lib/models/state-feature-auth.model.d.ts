import { FirebaseUserProfile } from './firebase-user.model';
import { QxBindingAgreement } from './binding-agreement.model';
export interface AuthState {
    user?: FirebaseUserProfile;
    agreementRecords?: QxBindingAgreement[];
}
export interface FeatureAuthState {
    user?: FirebaseUserProfile;
    agreementRecords?: QxBindingAgreement[];
}
export declare const initialFeatureAuthState: AuthState;
export declare const AUTH_FEATURE_KEY = "auth";
