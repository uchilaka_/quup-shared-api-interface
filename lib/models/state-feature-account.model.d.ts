import { QxBindingAgreement } from './binding-agreement.model';
import { FirebaseUserProfile } from './firebase-user.model';
export declare const FEATURE_ACCOUNT_SLICE = "account";
export interface FeatureAccountState {
    records: QxBindingAgreement[];
    firebaseUser?: FirebaseUserProfile;
    appUser?: any;
}
export declare const initialFeatureAccountState: FeatureAccountState;
