import { TimeRecord } from './time-record.model';
export declare const DRAFT_DOC_INDEX = "zDraftDocument";
export interface QxFirebaseObject {
    time_deleted?: TimeRecord;
    time_created?: TimeRecord;
    time_modified?: TimeRecord;
    /**
     * @TODO re-factor code as needed to switch this to a required field
     */
    authorUID?: string | null;
    id: string;
    /**
     * Temporary updates to this document that have not yet been saved
     */
    [DRAFT_DOC_INDEX]?: Partial<QxFirebaseObject> | any;
}
