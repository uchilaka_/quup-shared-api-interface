export interface DefaultHeaders {
    Accept?: string;
    Authorization?: string;
    'User-Agent'?: string;
    'Content-Type'?: string;
}
export interface KnownHeaders extends DefaultHeaders {
    [k: string]: any;
}
