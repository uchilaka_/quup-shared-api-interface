import { EventEmitter } from '@angular/core';
import { Dismissable } from './dismissable.model';
export declare enum DocumentResponse {
    ACCEPT = "ok",
    REJECT = "reject"
}
export declare abstract class InteractiveDocument extends Dismissable<InteractiveDocument> {
    /**
     * All interactive documents are dismissable by default
     */
    dismissable: boolean;
    /**
     * This event will be emitted when the user responds
     */
    userResponse: EventEmitter<DocumentResponse>;
}
