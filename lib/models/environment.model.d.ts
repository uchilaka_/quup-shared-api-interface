import { InjectionToken } from '@angular/core';
import { AppContext } from './context.model';
export declare const APP_CONFIG_TOKEN: InjectionToken<AppContext>;
