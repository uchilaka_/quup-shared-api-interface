export declare enum QxFirebaseAuthErrorCodes {
    EMAIL_ALREADY_EXISTS = "auth/email-already-exists"
}
export interface QxFirebaseAuthErrorInfo {
    code: QxFirebaseAuthErrorCodes | string;
    message: string;
    codePrefix?: string;
}
export interface QxFirebaseAuthError {
    Error: string;
    errorInfo: QxFirebaseAuthErrorInfo;
}
