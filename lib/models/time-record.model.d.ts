import { GenericFormInputOption } from './input-options.model';
export declare class TimeRecord {
    utc: string;
    unix: number;
    static NOW(timezone?: string): TimeRecord;
    static sanitize({ utc, unix }: TimeRecord): TimeRecord;
    constructor(utc?: string, unix?: number);
}
export declare const DURATION_INTERVALS: GenericFormInputOption[];
