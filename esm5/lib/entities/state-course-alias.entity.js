import { createEntityAdapter } from '@ngrx/entity';
export function sortCourseAliasById(e1, e2) {
    return e1.id - e2.id;
}
export function sortCourseAliasIds(id1, id2) {
    return id1 - id2;
}
export function reduceCourseAliasArrayToMap(map, courseAlias) {
    map[courseAlias.id] = courseAlias;
    return map;
}
export var selectCourseAliasEntityId = function (courseAlias) {
    return courseAlias.id;
};
export var courseAliasEntityAdapter = createEntityAdapter({
    sortComparer: sortCourseAliasById,
    selectId: selectCourseAliasEntityId
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGUtY291cnNlLWFsaWFzLmVudGl0eS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BxdXVwLW12cC1ueC9zaGFyZWQtYXBpLWludGVyZmFjZS8iLCJzb3VyY2VzIjpbImxpYi9lbnRpdGllcy9zdGF0ZS1jb3Vyc2UtYWxpYXMuZW50aXR5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLE9BQU8sRUFBRSxtQkFBbUIsRUFBOEIsTUFBTSxjQUFjLENBQUM7QUFRL0UsTUFBTSxVQUFVLG1CQUFtQixDQUFDLEVBQWtCLEVBQUUsRUFBa0I7SUFDeEUsT0FBTyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFFLENBQUM7QUFDdkIsQ0FBQztBQUVELE1BQU0sVUFBVSxrQkFBa0IsQ0FBQyxHQUFXLEVBQUUsR0FBVztJQUN6RCxPQUFPLEdBQUcsR0FBRyxHQUFHLENBQUM7QUFDbkIsQ0FBQztBQUVELE1BQU0sVUFBVSwyQkFBMkIsQ0FDekMsR0FBbUIsRUFDbkIsV0FBMkI7SUFFM0IsR0FBRyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsR0FBRyxXQUFXLENBQUM7SUFDbEMsT0FBTyxHQUFHLENBQUM7QUFDYixDQUFDO0FBRUQsTUFBTSxDQUFDLElBQU0seUJBQXlCLEdBQStCLFVBQ25FLFdBQTJCO0lBRTNCLE9BQU8sV0FBVyxDQUFDLEVBQUUsQ0FBQztBQUN4QixDQUFDLENBQUM7QUFFRixNQUFNLENBQUMsSUFBTSx3QkFBd0IsR0FFakMsbUJBQW1CLENBQWlCO0lBQ3RDLFlBQVksRUFBRSxtQkFBbUI7SUFDakMsUUFBUSxFQUFFLHlCQUF5QjtDQUNwQyxDQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBjcmVhdGVFbnRpdHlBZGFwdGVyLCBFbnRpdHlBZGFwdGVyLCBFbnRpdHlTdGF0ZSB9IGZyb20gJ0BuZ3J4L2VudGl0eSc7XG5pbXBvcnQgeyBJZFNlbGVjdG9yIH0gZnJvbSAnQG5ncngvZW50aXR5L3NyYy9tb2RlbHMnO1xuaW1wb3J0IHsgQ21zQ291cnNlQWxpYXMsIENvdXJzZUFsaWFzTWFwIH0gZnJvbSAnLi4vbW9kZWxzJztcblxuZXhwb3J0IGludGVyZmFjZSBReENvdXJzZUFsaWFzRW50aXR5U3RhdGUgZXh0ZW5kcyBFbnRpdHlTdGF0ZTxDbXNDb3Vyc2VBbGlhcz4ge1xuICBzZWxlY3RlZENvdXJzZUFsaWFzSWQ6IG51bWJlciB8IG51bGw7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBzb3J0Q291cnNlQWxpYXNCeUlkKGUxOiBDbXNDb3Vyc2VBbGlhcywgZTI6IENtc0NvdXJzZUFsaWFzKSB7XG4gIHJldHVybiBlMS5pZCAtIGUyLmlkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gc29ydENvdXJzZUFsaWFzSWRzKGlkMTogbnVtYmVyLCBpZDI6IG51bWJlcikge1xuICByZXR1cm4gaWQxIC0gaWQyO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gcmVkdWNlQ291cnNlQWxpYXNBcnJheVRvTWFwKFxuICBtYXA6IENvdXJzZUFsaWFzTWFwLFxuICBjb3Vyc2VBbGlhczogQ21zQ291cnNlQWxpYXNcbikge1xuICBtYXBbY291cnNlQWxpYXMuaWRdID0gY291cnNlQWxpYXM7XG4gIHJldHVybiBtYXA7XG59XG5cbmV4cG9ydCBjb25zdCBzZWxlY3RDb3Vyc2VBbGlhc0VudGl0eUlkOiBJZFNlbGVjdG9yPENtc0NvdXJzZUFsaWFzPiA9IChcbiAgY291cnNlQWxpYXM6IENtc0NvdXJzZUFsaWFzXG4pID0+IHtcbiAgcmV0dXJuIGNvdXJzZUFsaWFzLmlkO1xufTtcblxuZXhwb3J0IGNvbnN0IGNvdXJzZUFsaWFzRW50aXR5QWRhcHRlcjogRW50aXR5QWRhcHRlcjxcbiAgQ21zQ291cnNlQWxpYXNcbj4gPSBjcmVhdGVFbnRpdHlBZGFwdGVyPENtc0NvdXJzZUFsaWFzPih7XG4gIHNvcnRDb21wYXJlcjogc29ydENvdXJzZUFsaWFzQnlJZCxcbiAgc2VsZWN0SWQ6IHNlbGVjdENvdXJzZUFsaWFzRW50aXR5SWRcbn0pO1xuIl19