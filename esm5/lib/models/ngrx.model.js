var ErrorActionProp = /** @class */ (function () {
    function ErrorActionProp(error, length) {
        if (length === void 0) { length = 0; }
        this.error = error;
        this.length = length;
    }
    return ErrorActionProp;
}());
export { ErrorActionProp };
export function ErrorAction(error, length) {
    if (length === void 0) { length = 0; }
    return new ErrorActionProp(error, length);
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmdyeC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BxdXVwLW12cC1ueC9zaGFyZWQtYXBpLWludGVyZmFjZS8iLCJzb3VyY2VzIjpbImxpYi9tb2RlbHMvbmdyeC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNFLHlCQUFtQixLQUFhLEVBQVMsTUFBVTtRQUFWLHVCQUFBLEVBQUEsVUFBVTtRQUFoQyxVQUFLLEdBQUwsS0FBSyxDQUFRO1FBQVMsV0FBTSxHQUFOLE1BQU0sQ0FBSTtJQUFHLENBQUM7SUFDekQsc0JBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQzs7QUFFRCxNQUFNLFVBQVUsV0FBVyxDQUFDLEtBQWEsRUFBRSxNQUFVO0lBQVYsdUJBQUEsRUFBQSxVQUFVO0lBQ25ELE9BQU8sSUFBSSxlQUFlLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO0FBQzVDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJleHBvcnQgY2xhc3MgRXJyb3JBY3Rpb25Qcm9wIHtcbiAgY29uc3RydWN0b3IocHVibGljIGVycm9yPzogRXJyb3IsIHB1YmxpYyBsZW5ndGggPSAwKSB7fVxufVxuXG5leHBvcnQgZnVuY3Rpb24gRXJyb3JBY3Rpb24oZXJyb3I/OiBFcnJvciwgbGVuZ3RoID0gMCkge1xuICByZXR1cm4gbmV3IEVycm9yQWN0aW9uUHJvcChlcnJvciwgbGVuZ3RoKTtcbn1cbiJdfQ==