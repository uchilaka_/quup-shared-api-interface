import * as tslib_1 from "tslib";
import { Output, EventEmitter } from '@angular/core';
import { Dismissable } from './dismissable.model';
export var DocumentResponse;
(function (DocumentResponse) {
    DocumentResponse["ACCEPT"] = "ok";
    DocumentResponse["REJECT"] = "reject";
})(DocumentResponse || (DocumentResponse = {}));
var InteractiveDocument = /** @class */ (function (_super) {
    tslib_1.__extends(InteractiveDocument, _super);
    function InteractiveDocument() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        /**
         * All interactive documents are dismissable by default
         */
        _this.dismissable = true;
        /**
         * This event will be emitted when the user responds
         */
        _this.userResponse = new EventEmitter();
        return _this;
    }
    InteractiveDocument.propDecorators = {
        userResponse: [{ type: Output }]
    };
    return InteractiveDocument;
}(Dismissable));
export { InteractiveDocument };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50ZXJhY3RpdmUtZG9jLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQHF1dXAtbXZwLW54L3NoYXJlZC1hcGktaW50ZXJmYWNlLyIsInNvdXJjZXMiOlsibGliL21vZGVscy9pbnRlcmFjdGl2ZS1kb2MubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLE9BQU8sRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUVsRCxNQUFNLENBQU4sSUFBWSxnQkFHWDtBQUhELFdBQVksZ0JBQWdCO0lBQzFCLGlDQUFhLENBQUE7SUFDYixxQ0FBaUIsQ0FBQTtBQUNuQixDQUFDLEVBSFcsZ0JBQWdCLEtBQWhCLGdCQUFnQixRQUczQjtBQUVEO0lBQWtELCtDQUVqRDtJQUZEO1FBQUEscUVBYUM7UUFWQzs7V0FFRztRQUNILGlCQUFXLEdBQUcsSUFBSSxDQUFDO1FBRW5COztXQUVHO1FBRUgsa0JBQVksR0FBbUMsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7SUFDcEUsQ0FBQzs7K0JBRkUsTUFBTTs7SUFFVCwwQkFBQztDQUFBLEFBYkQsQ0FBa0QsV0FBVyxHQWE1RDtTQWJxQixtQkFBbUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBPdXRwdXQsIEV2ZW50RW1pdHRlciB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGlzbWlzc2FibGUgfSBmcm9tICcuL2Rpc21pc3NhYmxlLm1vZGVsJztcblxuZXhwb3J0IGVudW0gRG9jdW1lbnRSZXNwb25zZSB7XG4gIEFDQ0VQVCA9ICdvaycsXG4gIFJFSkVDVCA9ICdyZWplY3QnXG59XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBJbnRlcmFjdGl2ZURvY3VtZW50IGV4dGVuZHMgRGlzbWlzc2FibGU8XG4gIEludGVyYWN0aXZlRG9jdW1lbnRcbj4ge1xuICAvKipcbiAgICogQWxsIGludGVyYWN0aXZlIGRvY3VtZW50cyBhcmUgZGlzbWlzc2FibGUgYnkgZGVmYXVsdFxuICAgKi9cbiAgZGlzbWlzc2FibGUgPSB0cnVlO1xuXG4gIC8qKlxuICAgKiBUaGlzIGV2ZW50IHdpbGwgYmUgZW1pdHRlZCB3aGVuIHRoZSB1c2VyIHJlc3BvbmRzXG4gICAqL1xuICBAT3V0cHV0KClcbiAgdXNlclJlc3BvbnNlOiBFdmVudEVtaXR0ZXI8RG9jdW1lbnRSZXNwb25zZT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG59XG4iXX0=